package com.example.ksg007.projectwithkotlin

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.*
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import com.example.ksg007.projectwithkotlin.adapter.RecyclerViewAdapter
import com.example.ksg007.projectwithkotlin.entities.Website
import java.io.FileNotFoundException
import java.io.InputStream

class MainActivity : AppCompatActivity() {

    lateinit var adapter : RecyclerViewAdapter
    lateinit var recyclerViewID : RecyclerView
    lateinit var ImgGetPic: ImageView
    lateinit var bitmap : Bitmap
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        recyclerViewID = findViewById(R.id.recyclerViewID)
        recyclerViewID.layoutManager=LinearLayoutManager(this)
        adapter= RecyclerViewAdapter(this)
        recyclerViewID.adapter=adapter
        addWebsites()
    }
    private fun addWebsites() {
        val img = intArrayOf(R.drawable.web1, R.drawable.web2, R.drawable.web3, R.drawable.web4, R.drawable.web5, R.drawable.web6, R.drawable.web7)
        val title = arrayOf("វិទ្យាស្ថានជាតិពហុបច្ចេកវិទ្យាកម្ពុជា-NPIC - NATIONAL POLYTECHNIC INSTITUTE OF CAMBODIA", "វិទ្យាស្ថានបច្ចេកវិទ្យាភ្នំពេញ", "វិទ្យាស្ថានបច្ចេកវិទ្យាកម្ពុជា -INSTITUT DE TECHNOLOGIE DU CAMBODGE (ITC)", "វិទ្យាស្ថានស៊ីតិក", "វិទ្យាស្ថានបច្ចេកវិទ្យាកម្ពុជា", "វិទ្យាស្ថាន អភិវឌ្ឍន៍និស្សិត", "វិទ្យាស្ថានជាតិពាណិជ្ជសាស្រ្ដ")
        val tel = arrayOf("023 6376 633", "+855 23 90 00 49", "+855 23 88 03 70", "+855 23 88 06 12", "+855 23 88 03 70", "012 917 627", "+855 12 606 572")
        val url = arrayOf("http://www.npic.edu.kh", "https://www.facebook.com/ppitholding", "http://www.itc.edu.kh", "http://www.setecu.com", "http://www.itc.edu.kh/", "http://www.sdi-institute.com", "http://www.nib.edu.kh")
        val location = arrayOf(" ភូមិព្រៃពពេល, សង្កាត់សំរោងក្រោម, រាជធានីភ្នំពេញ", " Phnom Penh, No. 541, Street. 1003, Sangkat Phnom Penh Thmey, Khan Sen Sok\n", " Phnom Penh, Street. Confederation de la Russie Blvd (110)\n", " អាគារលេខ 86 អា មហាវិថីសហពន្ធ័រុស្ស៊ី (110), សង្កាត់ទឹកល្អក់ 1 ខណ្ឌទួលគោក រាជធានីភ្នំពេញ\n", " Phnom Penh, Street. Confederation de la Russie Blvd (110)\n", " No. 369, St. 138, 12154 Phnom Penh\n", " Phnom Penh, Street. Samdech Monireth Blvd (217)\n")
        var list = arrayListOf<Website>();
        for (i in 0..title.size - 1) {
            list.add(Website(img[i],null,title[i],tel[i],url[i],location[i]))
        }

        adapter.addMoreWebsites(list)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater.inflate(R.menu.mymenu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        var id= item!!.itemId
        if(id==R.id.addNewMenu){
            val alertDialog = AlertDialog.Builder(this@MainActivity)
            val mView = LayoutInflater.from(this@MainActivity).inflate(R.layout.add_manual, null)
            var btnCancel : Button = mView!!.findViewById(R.id.btnCancel)
            var btnAdd : Button = mView!!.findViewById(R.id.btnAdd)
            var title : EditText = mView!!.findViewById(R.id.tvTitle_Add)
            var url : EditText = mView!!.findViewById(R.id.tvWebsite_Add)
            var tel : EditText = mView!!.findViewById(R.id.tvTel_Add)
            var location : EditText = mView!!.findViewById(R.id.tvAddr_Add)
            var chooseImg : Button = mView!!.findViewById(R.id.btnChooseImg)
            ImgGetPic=mView!!.findViewById(R.id.imgView)
            val dialog = alertDialog.create()
            dialog.setView(mView)
            dialog.show()
            chooseImg.setOnClickListener(View.OnClickListener {
               // Toast.makeText(this,"sssssss",Toast.LENGTH_LONG).show()
                val pickerImageIntent = Intent(Intent.ACTION_PICK)
                pickerImageIntent.type = "image/*"
                startActivityForResult(pickerImageIntent,1)
            })
            btnCancel.setOnClickListener{
                dialog.dismiss()
            }
            btnAdd.setOnClickListener{
                adapter.addWebsite(Website(0,bitmap,title.text.toString(),tel.text.toString(),url.text.toString(),location.text.toString()),recyclerViewID,adapter)
                dialog.cancel()
            }
        }
        return super.onOptionsItemSelected(item)
    }
    fun mytest (){
        Toast.makeText(this,"asda",Toast.LENGTH_LONG).show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == 1) {
                val imgUri = data!!.getData()
                val inputStream: InputStream?
                try {
                    inputStream = contentResolver.openInputStream(imgUri!!)
                    // Bitmap bitmap= BitmapFactory.decodeStream(inputStream);
                    //                   Toast.makeText(this, ""+bitmap, Toast.LENGTH_SHORT).show();
                    bitmap = BitmapFactory.decodeStream(inputStream)
                    ImgGetPic.setImageBitmap(bitmap)
                    //                    imgview.setImageBitmap(bitmap);
                } catch (e: FileNotFoundException) {
                    e.printStackTrace()
                    Toast.makeText(this, "Unable to Pick Image", Toast.LENGTH_SHORT).show()
                }

            } else {
                Toast.makeText(this, "Request code didn't get", Toast.LENGTH_SHORT).show()
            }
            if(requestCode == 2){
                val imgUri = data!!.getData()
                val inputStream: InputStream?
                inputStream = contentResolver.openInputStream(imgUri!!)
                adapter.onActivityResult(2,-1,data,inputStream)
            }
        } else {
            ImgGetPic.setImageResource(R.drawable.empty_gallery)
            Toast.makeText(this, "You cancel picking up image...", Toast.LENGTH_SHORT).show()
        }
    }
}

package com.example.ksg007.projectwithkotlin.entities

import android.graphics.Bitmap

/**
 * Created by Mr.Pheng on 12-Nov--11--17.
 */

class Website(var image: Int, var bitmap: Bitmap?, var title: String?, var tel: String?, var url: String?, var location: String?) {

    override fun toString(): String {
        return "Website(title=$title, tel=$tel, url=$url, location=$location)"
    }
}

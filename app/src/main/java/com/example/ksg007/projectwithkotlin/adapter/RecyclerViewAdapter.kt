package com.example.ksg007.projectwithkotlin.adapter

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.example.ksg007.projectwithkotlin.R
import com.example.ksg007.projectwithkotlin.entities.Website
import java.io.FileNotFoundException
import java.io.InputStream

/**
 * Created by Mr.Pheng on 27-Dec--12--17.
 */
class RecyclerViewAdapter(val context: Context) : RecyclerView.Adapter<RecyclerViewAdapter.WebsiteViewHolder>() {

    private var list : MutableList<Website> = ArrayList<Website>()

//    private  var context : Context =
    lateinit var ImgGetPic: ImageView
     var bitmap : Bitmap? = null
    override fun onBindViewHolder(holder: WebsiteViewHolder?, position: Int) {
        holder!!.onBind(list[position])
    }

    override fun getItemCount(): Int =list.size

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): WebsiteViewHolder {
        var view = LayoutInflater.from(parent!!.context).inflate(R.layout.items_layout,parent,false)
        return WebsiteViewHolder(view)
    }
    public fun addMoreWebsites(list : List<Website>){
        this.list.addAll(list)
        notifyDataSetChanged()
    }
    public fun addWebsite(website: Website,recyclerView: RecyclerView,adapter: RecyclerViewAdapter){
        this.list.add(0,website)
        adapter.notifyItemInserted(0)
        recyclerView.smoothScrollToPosition(0);
    }

    inner class WebsiteViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        //    private val image: ImageView = itemView!!.findViewById(R.id.imgView)
        private val title: TextView = itemView!!.findViewById(R.id.tvTitle)
        private val tel: TextView = itemView!!.findViewById(R.id.tvTel)
        private val url: TextView = itemView!!.findViewById(R.id.tvWebsite)
        private val location: TextView = itemView!!.findViewById(R.id.tvLocation)
        private var image : ImageView = itemView!!.findViewById(R.id.imageViewItem)
        private val imgBtnOpt: ImageView =itemView!!.findViewById(R.id.imgOpt)
        fun onBind(website : Website){
//        image.setImageResource(website.image)
            var getImg : Int = website.image
            if(getImg != 0){
                image.setImageResource(getImg)
            }else{
                image.setImageBitmap(website.bitmap)
            }


            title.text = website.title
            tel.text=website.tel
            url.text=website.url
            location.text=website.location
            imgBtnOpt.setOnClickListener{
                val menu = PopupMenu(context, imgBtnOpt)
                menu.menuInflater.inflate(R.menu.popup_edit, menu.menu)
                menu.setOnMenuItemClickListener(PopupMenu.OnMenuItemClickListener {
                    menuItem -> when(menuItem.title) {
                    "Edit" -> {
                        val alertDialog = AlertDialog.Builder(context)
                        val mView = LayoutInflater.from(context).inflate(R.layout.update_layout, null)
                        var btnCancel : Button = mView!!.findViewById(R.id.btnCancel)
                        var btnUp : Button = mView!!.findViewById(R.id.btnUp)
                        var title : EditText = mView!!.findViewById(R.id.tvTitle_Up)
                        var url : EditText = mView!!.findViewById(R.id.tvWebsite_Up)
                        var tel : EditText = mView!!.findViewById(R.id.tvTel_Up)
                        var location : EditText = mView!!.findViewById(R.id.tvAddr_Up)
                        ImgGetPic = mView!!.findViewById(R.id.imgView)
                        var btnChoose : Button = mView!!.findViewById(R.id.btnChooseImg)
                        var position : Website = list.get(adapterPosition)
                        title.setText(position.title)
                        url.setText(position.url)
                        tel.setText(position.tel)
                        location.setText(position.location)
                        var check : Int
                        if(position.image != 0){
                            ImgGetPic.setImageResource(position.image)
                            check=position.image
                            bitmap=null
                        }else{
                            check=0
                            ImgGetPic.setImageBitmap(position.bitmap)
                        }
                        title.requestFocus()
                        val dialog = alertDialog.create()
                        dialog.setView(mView)
                        dialog.show()
                        btnChoose.setOnClickListener(View.OnClickListener {
                            val pickerImageIntent = Intent(Intent.ACTION_PICK)
                            pickerImageIntent.type = "image/*"
                            (context as Activity).startActivityForResult(pickerImageIntent, 2)
                            check=0
                        })
                        btnCancel.setOnClickListener{
                            dialog.dismiss()
                        }
                        btnUp.setOnClickListener{
                            list.set(adapterPosition, Website(check,bitmap,title.text.toString(),
                                    tel.text.toString(),
                                    url.text.toString(),
                                    location.text.toString()));
                            notifyItemChanged(adapterPosition)
                            Toast.makeText(context,"Item has updated...",Toast.LENGTH_LONG).show()
                            dialog.cancel()
                        }
                        false

                    }else-> {
                        list.removeAt(adapterPosition)
                        notifyItemRemoved(adapterPosition)
                        true
                    }



                }
//                    menuItem.title.toString()
                })
                menu.show()
            }
        }
    }

    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?, inputStream: InputStream?) {

        try {
            bitmap = BitmapFactory.decodeStream(inputStream)
            ImgGetPic.setImageBitmap(bitmap)
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        }

    }
}
